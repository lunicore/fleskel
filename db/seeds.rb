# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Keyword.create!(base: "kaka", definition: "Något man äter")
Keyword.create!(base: "bil", definition: "Något man kör")
Keyword.create!(base: "koda", definition: "Något man kodar")
Keyword.create!(base: "möte", definition: "Något man möter")