class CreateJoinTable < ActiveRecord::Migration
  def change
    create_join_table :keywords, :timelogs do |t|
      t.index [:keyword_id, :timelog_id]
      t.index [:timelog_id, :keyword_id]
    end
  end
end
