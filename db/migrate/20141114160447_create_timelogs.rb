class CreateTimelogs < ActiveRecord::Migration
  def change
    create_table :timelogs do |t|
      t.references :user, index: true
      t.datetime :start
      t.datetime :end
      t.integer :seconds
      t.text :description
      t.references :project, index: true

      t.timestamps
    end
  end
end
