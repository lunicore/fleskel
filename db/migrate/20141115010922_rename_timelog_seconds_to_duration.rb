class RenameTimelogSecondsToDuration < ActiveRecord::Migration
  def change
    rename_column :timelogs, :seconds, :duration
  end
end
