class CreateKeywords < ActiveRecord::Migration
  def change
    create_table :keywords do |t|
      t.text :definition
      t.string :base

      t.timestamps
    end
  end
end
