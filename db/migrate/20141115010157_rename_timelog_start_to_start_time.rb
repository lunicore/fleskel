class RenameTimelogStartToStartTime < ActiveRecord::Migration
  def change
    rename_column :timelogs, :start, :start_time
  end
end
