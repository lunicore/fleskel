class RemoveEndFromTimelog < ActiveRecord::Migration
  def change
    remove_column :timelogs, :end, :datetime
  end
end
