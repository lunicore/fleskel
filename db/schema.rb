# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141115010922) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "keyword_references", force: true do |t|
    t.integer  "keyword_id"
    t.integer  "timelog_id"
    t.integer  "character_index"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "keyword_references", ["keyword_id"], name: "index_keyword_references_on_keyword_id", using: :btree
  add_index "keyword_references", ["timelog_id"], name: "index_keyword_references_on_timelog_id", using: :btree

  create_table "keywords", force: true do |t|
    t.text     "definition"
    t.string   "base"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "keywords_timelogs", id: false, force: true do |t|
    t.integer "keyword_id", null: false
    t.integer "timelog_id", null: false
  end

  add_index "keywords_timelogs", ["keyword_id", "timelog_id"], name: "index_keywords_timelogs_on_keyword_id_and_timelog_id", using: :btree
  add_index "keywords_timelogs", ["timelog_id", "keyword_id"], name: "index_keywords_timelogs_on_timelog_id_and_keyword_id", using: :btree

  create_table "projects", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "timelogs", force: true do |t|
    t.integer  "user_id"
    t.datetime "start_time"
    t.integer  "duration"
    t.text     "description"
    t.integer  "project_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "timelogs", ["project_id"], name: "index_timelogs_on_project_id", using: :btree
  add_index "timelogs", ["user_id"], name: "index_timelogs_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
