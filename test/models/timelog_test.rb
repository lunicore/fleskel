require 'test_helper'

class TimelogTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  setup do
    @timelog = timelogs(:one)
  end

  test "keywords creation" do
    @timelog.description= "#keyword1 nothing"
    assert_difference('Keyword.count') do
      @timelog.save
    end
    @timelog2 = timelogs(:one)
    @timelog2.description= "#keyword1 something else"
    assert_difference('Keyword.count',0) do
      @timelog2.save
    end
  end
end
