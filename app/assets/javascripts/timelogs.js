// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(function ()  {
  /**
   * Method for toggling calendar-box on tab selection change
   */
  $('input[name=date_select]').change(function (e) {
    if ($(e.target).attr('id') === "date_select_2") {
      $('#calendar-box').toggleClass('collapsed');
    } else {
      $('#calendar-box').addClass('collapsed');
      $("#calendar-smalldate").addClass('hidden');;
    }
  });

  $('#calendar-label').click(function(event) {
    if ($('#date_select_2').prop('checked') &&
        $('#calendar-box').hasClass('collapsed')) {
      $('#calendar-box').removeClass('collapsed');
    };
  });

  $("#datepicker").datepicker()
    .on('changeDate', function(e) {
      var date = $("#datepicker").datepicker('getDate');
      $("#timelog_date").val(date);
      $("#calendar-smalldate").removeClass('hidden');;
      $("#calendar-smalldate").text(date.getDate() + '/' + date.getMonth());
      $('#calendar-box').addClass('collapsed');
    });
})

$(function() {
  doTheDeed();
});

var autoCompleteArray = [];
var usersArray = [];
function doTheDeed() {
  var ajaxJson = $.getJSON("/keywords.json", function(data) {
    $.each(data,function(key,val) { autoCompleteArray.push(data[key].base);});
  });
  var userAjaxJson = $.getJSON("/users.json",function(data) {
    $.each(data,function(key,val) {
      var strName = data[key].name;
      var strConcat = strName.toLowerCase();
      strConcat = strConcat.replace(/ /g,'');
      usersArray.push(strConcat);
    });
  });

  $("#timelog_description").textcomplete([
    {
        mentions: autoCompleteArray,
        match: /\B#(\w*)$/,
        search: function (term, callback) {
            callback($.map(this.mentions, function (mention) {
                return mention.indexOf(term) === 0 ? mention : null;
            }));
        },
        index: 1,
        replace: function (mention) {
            return '#' + mention + ' ';
        }
    },{
      mentions: usersArray,
      match: /\B@(\w*)$/,
      search: function (term,callback) {
        callback($.map(this.mentions,function(mention) {
            return mention.indexOf(term.toLowerCase())===0 ? mention : null;
        }));
      },
      index: 1,
      replace: function (mention) {
        return '@' + mention + ' ';
      }
    }
], { appendTo: 'body' });
}


$(function() {
  $("#timelog_description").on('keyup',function(){
    var lastChar = $(this).val().slice(-1);
    // Check if char is end of word
    if(lastChar == '.' || lastChar == ',' || lastChar == ' '){
      // Get last word in textfield
      var lastWord = lastWordFunction($(this).val());
      if (lastWord!=null && lastWord.substring(0,1) == "#") {
        return;
      }
      if($.inArray(lastWord, autoCompleteArray) > -1){
        //Ask if user missed a hashtag for the word 'lastWord'
        console.log("TODO: Ask if user missed a hashtag for the word '" + lastWord + "'");
      }
    }
  });
});

var ajaxJson = $.ajax({
        url: "keywords/create.json",
        type: "POST",
        data: {base: lastWord, description: newdescription},
        dataType: "JSON"
      });

function lastWordFunction(o) {
  return (""+o).replace(/[\s-]+$/,'').split(/[\s-]/).pop();
}


