json.array!(@timelogs) do |timelog|
  json.extract! timelog, :id, :user_id, :start_time, :duration, :description, :project_id
  json.url timelog_url(timelog, format: :json)
end
