class KeywordsController < ApplicationController
  before_action :set_keyword, only: [:show, :edit, :update, :destroy]

  respond_to :html,:json

  def index
    @keywords = Keyword.all.order(:base)
    respond_with(@keywords)
  end

  def show
    respond_with(@keyword)
  end

  def new
    @keyword = Keyword.new
    respond_with(@keyword)
  end

  def edit
  end

  def create
    @keyword = Keyword.new(keyword_params)
    @keyword.save
    respond_with(@keyword)
  end

  def update
    @keyword.update(keyword_params)
    respond_with(@keyword)
  end

  def destroy
    @keyword.destroy
    respond_with(@keyword)
  end

  private
    def set_keyword
      @keyword = Keyword.find(params[:id])
    end

    def keyword_params
      params.require(:keyword).permit(:definition, :base)
    end
end
