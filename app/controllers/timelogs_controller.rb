class TimelogsController < ApplicationController
  before_action :set_timelog, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @timelogs = Timelog.all
    respond_with(@timelogs)
  end

  def show
    respond_with(@timelog)
  end

  def new
    @timelog = Timelog.new
    respond_with(@timelog)
  end

  def edit
  end

  def create
    case params[:date_select]
    when "0"
      date = Date.yesterday
    when "1"
      date = Date.today
    when "2"
      date = params[:timelog][:date].to_date
    end
    params[:timelog][:start_time] = date
    params[:timelog][:user_id]  = current_user.id
    @timelog = Timelog.new(timelog_params)
    @timelog.save
    respond_with(@timelog)
  end

  def update
    @timelog.update(timelog_params)
    respond_with(@timelog)
  end

  def destroy
    @timelog.destroy
    respond_with(@timelog)
  end

  private
    def set_timelog
      @timelog = Timelog.find(params[:id])
    end

    def timelog_params
      params.require(:timelog).permit(:start_time, :duration, :description, :project_id, :user_id)
    end
end
