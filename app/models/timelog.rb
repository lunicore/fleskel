class Timelog < ActiveRecord::Base
  belongs_to :user
  belongs_to :project

  before_save :identify_keywords

  has_and_belongs_to_many :keywords

  validates :user,        presence: true
  validates :project,     presence: true
  validates :description, presence: true
  validates :start_time,  presence: true
  validates :duration,    presence: true,
                          numericality: {:greater_than => 0}

  def identify_keywords
    keywords = description.scan(/#\p{Word}+/).uniq.map { |kw| kw[1..-1] }

    keywords.each do |base|
      # TODO: stem base
      kw = Keyword.find_or_create_by(base: base)
      self.keywords << kw
    end
  end
end
