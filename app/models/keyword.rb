class Keyword < ActiveRecord::Base
  # TODO: keywords borde ha synonymer

  has_and_belongs_to_many :timelogs

  validates :base, presence: true
end
