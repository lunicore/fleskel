class Project < ActiveRecord::Base
  has_many :timelogs
  has_many :keywords, through: :timelogs

  validates :name, presence: true

end
