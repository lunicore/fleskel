= FLESKELL =


== Demo ==
http://fleskell.herokuapp.com/

== Development ==

```sh
	vagrant up
 	vagrant ssh
```

=== Start up ===

```
	bundle install
	rake db:create db:migrate db:seed
```